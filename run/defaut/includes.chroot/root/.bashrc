#redirection: http://abs.traduc.org/abs-5.0-fr/ch19.html
#if: http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_07_01.html

#### PERSONALISATION : DEBUT #############################################
#### colorisation #############################################
BOLD=`tput smso`
NOBOLD=`tput rmso`

#NORMAL="\[\e[0m\]"
BLACK=`tput setaf 0`
#RED="\[\e[1;31m\]"
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
CYAN=`tput setaf 4`
MAGENTA=`tput setaf 5`
BLUE=`tput setaf 6`
WHITE=`tput setaf 7`

NORMALcouleur=$WHITE
INFOcouleur=$BLUE
COMcouleur=$YELLOW
WARNcouleur=$RED
TITRE1couleur=$GREEN
TITRE2couleur=$MAGENTA

export BOLD
export NOBOLD
export BLACK
export RED
export GREEN
export YELLOW
export CYAN
export MAGENTA
export BLUE
export WHITE

export NORMALcouleur
export INFOcouleur
export WARNcouleur
export TITRE1couleur
export TITRE2couleur

#### personalisation du prompt #############################################
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u@\[\033[1;32m\]\h\[\033[00;00m\]:\[\033[01;34m\]\w\[\033[01;37m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# si dans un xterm afficher le titre au format user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac
PS1="\t $PS1"
unset color_prompt force_color_prompt

#### personalisation des alias #############################################
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'

#ln -s /run/shm  /home/pascal/  2>/dev/null

#### PERSONALISATION : FIN #############################################
