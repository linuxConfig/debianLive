#!/bin/sh

BUILDLOG=./build.log

### variable ###
ARCHITECTURE="i386"; #"i386"|"amd64"
DISTRIBUTION="jessie"; # "jessie"|"stable"|"testing"|"sid"
isBACKPORT="true"; # "true"|"false" #pour stable et old
CHROOT_FILESYSTEM="squashfs"; #"squashfs"|"ext3"
LINUX_FLAVOURS='586'

VERBOSE=''; # ''|'--verbose'

VMLINUZ_NAME="$ARCHITECTURE-$LINUX_FLAVOURS-vmlinuz"
INITRD_NAME="$ARCHITECTURE-$LINUX_FLAVOURS-initrd.img"
SQUASHFS_NAME="$PROJETNOM-$ARCHITECTURE-$LINUX_FLAVOURS-filesystem.squashfs"

#################
# CONFIGURATION #
#################

### live-boot ###
echo ""
echo "${CYAN}Configuration de live-boot${WHITE}"

## man live-boot
LIVE_BOOT_DEBUG="debug"          #''|'debug'
LIVE_BOOT_FETCH=""               # ''|'fetch=http://xxx'
LIVE_BOOT_VERIFY_CHECKSUM=""     #''|'verify-checksums'
LIVE_BOOT_NOEJECT="noeject"      #''|'noeject'
LIVE_BOOT_SILENT=""              #''|'silent'
LIVE_BOOT_UNION="union=aufs"     #'union=aufs'|'union=unionfs'
LIVE_BOOT_TODISK=""              #''|'todisk=DEVICE'
LIVE_SWAP_=""                    #''|'swap=true'
#LIVE_BOOT_="" #''|''

LIVE_BOOT_PERSISTENCE='persistence' #''|'nopersistence'|'persistence'|''|''
LIVE_BOOT_PERSISTENCE_PARAM="$LIVE_BOOT_PERSISTENCE"

LIVE_BOOT_PARAM="$LIVE_BOOT_DEBUG $LIVE_BOOT_FETCH $LIVE_BOOT_VERIFY_CHECKSUM $LIVE_BOOT_NOEJECT $LIVE_BOOT_SILENT $LIVE_BOOT_UNION $LIVE_BOOT_PERSISTENCE_PARAM "


### live-config ###
echo "${CYAN}Configuration de live-config${WHITE}"

#live-config.components
#live-config.nocomponents
#live-config.debconf-preseed
#live-config.hostname
#live-config.username
#live-config.user-default-groups
#live-config.user-fullname
#live-config.locales
#live-config.timezone
#live-config.keyboard-model
#live-config.keyboard-layout
#live-config.keyboard-variants
#live-config.keyboard-options
#live-config.sysv-rc=SERVICE1,SERVICE2 ... SERVICEn
#live-config.utc=yes|no
#live-config.x-session-manager=X_SESSION_MANAGER
#live-config.xorg-driver=DRIVER_PAR_XORG
#live-config.xorg-resolution=1024x768
#live-config.wlan-driver=DRIVER_PAR_WLAN
#live-config.hooks=filesystem|medium|URL1|URL2| ... |URL

#live-config.noroot (no sudo)
#live-config.noautologin
#live-config.nottyautologin
#live-config.nox11autologin

#live-config.debug


LIVE_CONFIG_CONFIG='live-config.config'
LIVE_CONFIG_COMPONENTS='live-config.components'

LIVE_CONFIG_HOSTNAME="live-config.hostname=legral_live"

LIVE_CONFIG_USERNAME="live-config.username=user"
LIVE_CONFIG_USER_FULLNAME='live-config.user-fullname="Utilisateur_Libre"'
#LIVE_CONFIG_USER_FULLNAME=''
LIVE_CONFIG_USER_DEFAULT_GROUPS='live-config.user-default-groups=audio,cdrom,dip,floppy,video,plugdev,netdev,powerdev,scanner,bluetooth,fuse';
LIVE_CONFIG_USERINFO="$LIVE_CONFIG_USERNAME $LIVE_CONFIG_USER_FULLNAME $LIVE_CONFIG_USER_DEFAULT_GROUPS"

LIVE_CONFIG_NOROOT='' # '' | 'live-config.noroot'


LIVE_CONFIG_LOCALES="locales=fr_FR.UTF-8"
LIVE_CONFIG_TIMEZONE='live-config.timezone=Europe/Paris'
LIVE_CONFIG_UTC="live-config.utc=yes"
LIVE_CONFIG_KEYBOARD="keyboard-layouts=fr"
LIVE_CONFIG_X_SESSION_MANAGER=""
LIVE_CONFIG_HOOKS=""

LIVE_CONFIG_PARAM="$LIVE_CONFIG_CONFIG $LIVE_CONFIG_COMPONENTS $LIVE_CONFIG_NOROOT $LIVE_CONFIG_HOSTNAME $LIVE_CONFIG_USERINFO $LIVE_CONFIG_LOCALES $LIVE_CONFIG_TIMEZONE $LIVE_CONFIG_UTC $LIVE_CONFIG_KEYBOARD $LIVE_CONFIG_X_SESSION_MANAGER $LIVE_CONFIG_HOOKS"


isDEBIAN_INSTALLER='true' #"true"|"live" #live:clone le systeme live qui sert à l'installation
isDEBIAN_INSTALLER_GUI='true' #'true"|"false'
BOOT_APPEND_INSTALL='locales=fr_FR.UTF-8' #'locales=fr_FR.UTF-8'

isAPT_RECOMMENDS='true' #'true"|"false'
isBUILD_WITH_CHROOT='true' #'true"|"false'

isCACHE='true' #'true"|"false'
isCACHE_INDICES='true' #'true"|"false'
isCACHE_PACKAGES='true' #'true"|"false'

BINARY_IMAGES='hdd-usb' #'hdd-usb'|''
BINARY_FILESYSTEM='fat32' #'fat32"|"'
BOOTLOADER='none' #'none"|"'
WIN32_LOADER='false' #'false"|"'

isMEMTEST='none' #'none"|"'

DEFCONF_PRIORITY='high' #'high"|"'


