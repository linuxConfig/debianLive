#!/bin/sh

# son rep de travail est le rep du script ./build/$PROJETNOM/ ?


#######################
### modif generique ###
#######################
echo ""
echo ""
echo "${GREEN}Ajout des hooks generaux${WHITE}"
add_template_lib public general hooks 0000-begin.hook.chroot
add_template_lib public general hooks 1001-del_usr_share_doc.hook.chroot

echo ""
echo "${GREEN}Ajout des package-lists generaux  ${WHITE}"


########################
### utilisateur:root ###
########################
echo ""
echo "${GREEN}Ajout de utilisateur:user ${WHITE}"
add_template_home_utilisateurs public root
add_template_home_lib public root root-autologin-tty2


########################
### utilisateur:user ###
########################
echo ""
echo "${GREEN}Ajout de utilisateur:user ${WHITE}"
add_template_home_utilisateurs public user
add_template_home_lib public user user-autologin-tty1


#####################################
### utilisateur:user-mediatheques ###
### modifie l'utilisateur 'user'  ###
#####################################
echo ""
echo "${GREEN}Ajout de utilisateur:user-mediatheques ${WHITE}"
add_template_home_utilisateurs public  user-mediatheques
add_template_home_utilisateurs private user-mediatheques


##############################
### collection: legralNet  ###
##############################
echo ""
echo "${GREEN}Collection: Ajout de legralNet ${WHITE}"

add_template_theme public legralNet-base
#add_template_theme public legralNet-X11-base


######################
###  package-lists ###
# Connaitre les task: apt-cache search --names-only ^task-
# lb config noauto --tasks "ssh-server task-lxde-desktop task-french"
######################
echo ""
echo ""
echo "${GREEN}Ajout des package-lists ${WHITE}"
add_template_lib public general package-lists minimal.list.chroot
#add_template_lib public general package-lists firmware.list.chroot
#add_template_lib public general package-lists installer.list.chroot


echo "${BOLD} logo boot ${NOBOLD}"
#add_template_lib public general package-lists boot-logo-welcome2l.list.chroot #dependance:


echo "${BOLD} localisation ${NOBOLD}"


echo "${BOLD} adminSys ${NOBOLD}"
add_template_lib public general package-lists adminDisque.list.chroot
#add_template_lib public general package-lists legralNet-monitoring.list.chroot
#cp -RHlnfv ../../includes.chroot/monitoring/* ./config/includes.chroot/
#add_template_lib public general package-lists legralNet-compilation.list.chroot


echo "${BOLD} reseau/www ${NOBOLD}"
#add_template_lib public general package-lists legralNet-nas.list.chroot
#add_template_lib public general package-lists legralNet-legralNet.list.chroot
#add_template_lib public general package-lists legralNet-lighttpd.list.chroot
#add_template_lib public general package-lists legralNet-piwigo.list.chroot
#add_template_lib public general package-lists legralNet-legralVpn.list.chroot


echo "${BOLD} hardware ${NOBOLD}"
add_template_lib public general package-lists hardware-utils.list.chroot


echo "${BOLD} securite ${NOBOLD}"
add_template_lib public general package-lists securite.list.chroot

add_template_lib public general package-lists antivirus.list.chroot
#add_template_lib public general package-lists antivirus-gui.list.chroot



echo "${BOLD} Serveur d'Affichage - Display Manager   ${NOBOLD}"
#add_template_lib public general package-lists X11.list.chroot

echo "${BOLD} Gestionnaire de fenetre  - Window Manager ${NOBOLD}"
#add_template_lib public general package-lists openbox.list.chroot

echo "${BOLD} Interface graphique - Graphical User Interface${NOBOLD}"
#add_template_lib public general package-lists legralNet-lxde


echo "${BOLD} bureautique ${NOBOLD}"
#add_template_lib public general package-lists legralNet-bureautique
#add_template_lib public general package-lists legralNet-videos
#add_template_lib public general package-lists legralNet-videosMontages
#add_template_lib public general package-lists legralNet-blender
#add_template_lib public general package-lists legralNet-vm
#add_template_lib public general package-lists legralNet-wine


echo "${BOLD} Jeux ${NOBOLD}"
#add_template_lib public general package-lists legralNet-jeux
#add_template_lib public general package-lists legralNet-jeux-enfant
#add_template_lib public general package-lists legralNet-jeux-simulation


echo "${BOLD} Divers ${NOBOLD}"
#add_template_lib public general package-lists legralNet-gnomeUtility
#add_template_lib public general package-lists pascal



