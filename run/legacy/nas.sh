#!/bin/sh

# son rep de travail est le rep du script ./build/$PROJETNOM/ ?
#echo "PROJETNOM:" $PROJETNOM
echo "repertoire courant: $PWD" 


########  contenu personalise du live ########
#echo "${TITRE2couleur} ajout de la structure chroot de  monitoring  ${NORMALcouleur}"
#cp -RHlnfv ../../includes.chroot/monitoring/* ./config/includes.chroot/

#echo "${GREEN} suppression des liens des package-lists preexistant ${WHITE}"
#rm -R ./config/package-lists
#mkdir -p ./config/package-lists

echo "${GREEN}Ajout des package-lists ${WHITE}"

packageLink0(){
   if [ -f "../../package-lists/${1}.list.chroot" ] ;then
   echo "${GREEN} $1 ${WHITE}"
   link ../../package-lists/${1}.list.chroot ./config/package-lists/${1}.list.chroot;
   else
      echo "${GREEN} $1 $RED ../../package-lists/${1}.list.chroot inexistant$WHITE"
   fi
}


#pl='legralNet-minimal';echo "${GREEN} $pl ${WHITE}"
#link ../../package-lists/${pl}.list.chroot		./config/package-lists/${pl}.list.chroot

#packageLink NoneExist
packageLink legralNet-minimal;
packageLink firmware
packageLink legralNet-adminDisque
packageLink legralNet-monitoring
packageLink legralNet-nas
packageLink legralNet-lighttpd
packageLink legralNet-piwigo

echo "${GREEN}lancement de lb config${WHITE}"
. ../../config/lb-hdd-usb.sh

