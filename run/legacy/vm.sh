#!/bin/bash

##on recupere le nom du script
#PROJETNOM=${0##*/}
echo "PROJETNOM:" $PROJETNOM
echo "repertoire courant: $PWD" 


########  contenu personalise du live ########
echo "${GREEN}Ajout des package-lists ${WHITE}"
link ../../package-lists/legralNet-minimal.list.chroot		./config/package-lists/legralNet-minimal.list.chroot
link ../../package-lists/firmware.list.chroot			./config/package-lists/firmware.list.chroot
link ../../package-lists/legralNet-monitoring.list.chroot			./config/package-lists/legralNet-monitoring.list.chroot
link ../../package-lists/legralNet-adminDisque.list.chroot	./config/package-lists/legralNet-adminDisque.list.chroot

link ../../package-lists/legralNet-X11.list.chroot		./config/package-lists/legralNet-X11.list.chroot
link ../../package-lists/legralNet-openbox.list.chroot		./config/package-lists/legralNet-openbox.list.chroot

link ../../package-lists/legralNet-vm.list.chroot		./config/package-lists/legralNet-vm.list.chroot

#link ../../package-lists/legralNet-gnomeUtility.list.chroot	./config/package-lists/legralNet-gnomeUtility.list.chroot

#link ../../package-lists/legralNet-bureautique.list.chroot	./config/package-lists/legralNet-bureautique.list.chroot
#link ../../package-lists/legralNet-videos.list.chroot		./config/package-lists/legralNet-videos.list.chroot
#link ../../package-lists/legralNet-videosMontages.list.chroot	./config/package-lists/legralNet-videosMontages.list.chroot



#cp -fl ../../package-lists/* ./config/package-lists/

echo "${GREEN}lancement de lb config${WHITE}"

echo "${CYAN}configuration des mirroirs${WHITE}"
lb config noauto \
	--cache true	--cache-indices false	--cache-packages true \
	\
	--mirror-bootstrap		http://127.0.0.1:7980/install/linux/mirroirs/sites/mirror/ftp.fr.debian.org/debian/ \
	--mirror-binary			http://127.0.0.1:7980/install/linux/mirroirs/sites/mirror/ftp.fr.debian.org/debian/ \
	--mirror-chroot			http://127.0.0.1:7980/install/linux/mirroirs/sites/mirror/ftp.fr.debian.org/debian/ \
	"${@}"

#lb config noauto \
#	--cache true	--cache-indices false	--cache-packages true \
#	--parent-mirror-bootstrap	http://ftp.fr.debian.org/debian/ \
#	--parent-mirror-binary 		http://ftp.fr.debian.org/debian/ \
#	--parent-mirror-chroot		http://ftp.fr.debian.org/debian/ \
#	\
#	--mirror-bootstrap			http://ftp.fr.debian.org/debian/ \
#	--mirror-binary			http://ftp.fr.debian.org/debian/ \
#	--mirror-chroot			http://ftp.fr.debian.org/debian/ \
#	\
#	--parent-mirror-chroot-updates	http://ftp.fr.debian.org/debian/ \
#	--parent-mirror-chroot-backports	http://ftp.fr.debian.org/debian/ \
#	\
#	--parent-mirror-binary-security	http://ftp.fr.debian.org/debian/ \
#	--parent-mirror-binary-updates	http://ftp.fr.debian.org/debian/ \
#	--parent-mirror-binary-backports	http://ftp.fr.debian.org/debian/ \
#	"${@}"
#	--mirror-debian-installer	http://ftp.fr.debian.org/debian/ \

echo "${CYAN}configuration generale${WHITE}"
lb config noauto \
	--system live \
	--mode debian \
	--archive-areas "main contrib" \
	-a i386 \
	-d wheezy \
	--binary-images  "hdd" \
	--binary-filesystem "fat32" \
	--build-with-chroot true \
	--bootloader syslinux \
	--debian-installer false \
\
	--apt-recommends true \
\
	--bootappend-live "boot=live config noprompt persistent locales=fr_FR.UTF-8 keyboard-layouts=fr " \
\
	--tasks "ssh-server task-french"    \
	--memtest none \
	--win32-loader false \
\
	"${@}"
#	--tasks "ssh-server task-lxde-desktop task-french"    \
#	--archive-areas "main contrib non-free" \

########  contenu personalise du live:fin ########

