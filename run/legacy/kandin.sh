#!/bin/bash

##on recupere le nom du script
#PROJETNOM=${0##*/}
echo "PROJETNOM:" $PROJETNOM
echo "repertoire courant: $PWD" 


########  contenu personalise du live ########
echo "${GREEN}Ajout des package-lists ${WHITE}"
link ../../package-lists/legralNet-minimal.list.chroot		./config/package-lists/legralNet-minimal.list.chroot
link ../../package-lists/firmware.list.chroot			./config/package-lists/firmware.list.chroot
link ../../package-lists/legralNet-adminDisque.list.chroot	./config/package-lists/legralNet-adminDisque.list.chroot

link ../../package-lists/legralNet-X11.list.chroot		./config/package-lists/legralNet-X11.list.chroot

#link ../../package-lists/legralNet-openbox.list.chroot		./config/package-lists/legralNet-openbox.list.chroot
link ../../package-lists/legralNet-lxde.list.chroot         ./config/package-lists/legralNet-lxde.list.chroot

link ../../package-lists/legralNet-bureautique.list.chroot	./config/package-lists/legralNet-bureautique.list.chroot
link ../../package-lists/legralNet-videos.list.chroot		./config/package-lists/legralNet-videos.list.chroot
link ../../package-lists/legralNet-videosMontages.list.chroot	./config/package-lists/legralNet-videosMontages.list.chroot
link ../../package-lists/legralNet-gimp.list.chroot		./config/package-lists/legralNet-gimp.list.chroot
link ../.././package-lists/legralNet-imageTools.list.chroot     ./config/package-lists/legralNet-imageTools.list.chroot


echo "cp ./package-lists/joelle/* ./$PROJETNOM/config/package-lists/"
#cp -fl ../../package-lists/* ./config/package-lists/

echo "${GREEN}lancement de lb config${WHITE}"

#lb config noauto \
#        --mirror-bootstrap http://127.0.0.1:7980/install/linux/mirroirs/sites/mirror/ftp.fr.debian.org/debian/ \
#        --mirror-binary http://127.0.0.1:7980/install/linux/mirroirs/sites/mirror/ftp.fr.debian.org/debian/ \
#        --mirror-chroot http://127.0.0.1:7980/install/linux/mirroirs/sites/mirror/ftp.fr.debian.org/debian/ \



lb config noauto \
	--system live \
	--archive-areas "main contrib non-free" \
	-a i386 \
	-d wheezy \
	--binary-images "hdd" \
	--binary-filesystem "fat32" \
	--bootloader "syslinux" \
	--build-with-chroot true \
	--bootappend-live "boot=live config  noeject persistence locales=fr_FR.UTF-8 keyboard-layouts=fr " \
	"${@}"

lb config noauto \
        --apt-recommends true \
        --apt-indices false  \
	--cache true    --cache-indices false   --cache-packages true \
	"${@}"

lb config noauto \
        --memtest none \
        --debian-installer false \
        --win32-loader false \
        "${@}"

lb config noauto \
	--cache true	--cache-indices false	--cache-packages true \
	--tasks "ssh-server task-french task-lxde-desktop"    \
        "${@}"
#	--tasks "ssh-server task-lxde-desktop task-french"    \
#	--archive-areas "main contrib non-free" \

########  contenu personalise du live:fin ########

