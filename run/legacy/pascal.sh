#!/bin/sh

# son rep de travail est le rep du script ./build/$PROJETNOM/ ?
echo "repertoire courant: $PWD" 

echo "${GREEN}Ajout des package-lists ${WHITE}"

#packageLink0(){
#   if [ -f "../../package-lists/${1}.list.chroot" ] ;then
#   echo "${GREEN} $1 ${WHITE}"
#   link ../../package-lists/${1}.list.chroot ./config/package-lists/${1}.list.chroot;
#   else
#      echo "${GREEN} $1 $RED ../../package-lists/${1}.list.chroot inexistant$WHITE"
#   fi
#}


#packageLink NoneExist
packageLink firmware

packageLink legralNet-minimal;

echo "${BOLD} adminSys ${NOBOLD}"
packageLink legralNet-adminDisque
packageLink legralNet-monitoring
#cp -RHlnfv ../../includes.chroot/monitoring/* ./config/includes.chroot/
packageLink legralNet-compilation



echo "${BOLD} reseau/www ${NOBOLD}"
packageLink legralNet-nas
packageLink legralNet-legralNet
#packageLink legralNet-lighttpd
#packageLink legralNet-piwigo
packageLink 

echo "${BOLD} Interface graphique ${NOBOLD}"
packageLink legralNet-X11
#packageLink openbox
packageLink legralNet-lxde
#ackageLink legralNet-
#ackageLink legralNet-

echo "${BOLD} bureautique ${NOBOLD}"
packageLink legralNet-bureautique
packageLink legralNet-videos
packageLink legralNet-videosMontages
packageLink legralNet-blender

echo "${BOLD} Divers ${NOBOLD}"
#packageLink legralNet-gnomeUtility
packageLink pascal


echo "${GREEN}lancement de lb config${WHITE}"
. ../../config/lb-hdd-usb.sh
