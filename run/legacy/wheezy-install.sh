#!/bin/bash

##on recupere le nom du script
#PROJETNOM=${0##*/}
echo "PROJETNOM:" $PROJETNOM
echo "repertoire courant: $PWD" 


########  contenu personalise du live ########
echo "${GREEN}Ajout des package-lists ${WHITE}"
link ../../package-lists/legralNet-minimal.list.chroot		./config/package-lists/legralNet-minimal.list.chroot
link ../../package-lists/firmware.list.chroot			./config/package-lists/firmware.list.chroot
link ../../package-lists/legralNet-adminDisque.list.chroot	./config/package-lists/legralNet-adminDisque.list.chroot

link ../../package-lists/legralNet-X11.list.chroot		./config/package-lists/legralNet-X11.list.chroot
link ../../package-lists/legralNet-openbox.list.chroot		./config/package-lists/legralNet-openbox.list.chroot

link ../../package-lists/legralNet-gnomeUtility.list.chroot	./config/package-lists/gnomeUtility.list.chroot
echo "cp ./package-lists/wheezy-install/* ./$PROJETNOM/config/package-lists/"
#cp -fl ../../package-lists/* ./config/package-lists/

echo "${GREEN}lancement de lb config${WHITE}"
lb config noauto \
	--mirror-bootstrap http://127.0.0.1:7980/install/linux/mirroirs/sites/mirror/ftp.fr.debian.org/debian/ \
	--mirror-binary    http://127.0.0.1:7980/install/linux/mirroirs/sites/mirror/ftp.fr.debian.org/debian/ \
	--mirror-chroot    http://127.0.0.1:7980/install/linux/mirroirs/sites/mirror/ftp.fr.debian.org/debian/ \
	"${@}"

lb config noauto \
	debconf_frontend dialog \
	debconf-priority high \
	debian-installer live \
	debian-installer-gui false \
	"${@}"
#debian-installer-preseedfile ./config/binary_debian-installer/pressed.cfg \

lb config noauto \
	--system live \
	--archive-areas "main contrib" \
	-a i386 \
	-d wheezy \
	--binary-images iso-hybrid \
	--binary-filesystem fat32 \
	--build-with-chroot true \
\
	--cache false	--cache-indices false	--cache-packages true \
\
	--apt-recommends true \
	--bootloader syslinux  \
\
	--bootappend-live "boot=live config noprompt  persistent locales=fr_FR.UTF-8 keyboard-layouts=fr " \
\
	--tasks "ssh-server task-french"    \
	--memtest none \
	--win32-loader false \
\
        "${@}"
#	--tasks "ssh-server task-lxde-desktop task-french"    \

########  contenu personalise du live:fin ########

