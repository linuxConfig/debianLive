#!/bin/bash

##on recupere le nom du script
#PROJETNOM=${0##*/}
#ce programme est lancer avec la commande fournit par config.sh
# son rep de travail est le rep du script ./build/$PROJETNOM/ ?
echo "PROJETNOM:" $PROJETNOM
echo "repertoire courant: $PWD" 


########  contenu personalise du live ########
#echo "${TITRE2couleur} ajout de la structure chroot de  monitoring  ${NORMALcouleur}"
#cp -RHlnfv ../../includes.chroot/monitoring/* ./config/includes.chroot/

# a faire supprimer les link prexistant
echo "${GREEN} suppression des liens des package-lists preexistant ${WHITE}"
rm -R ./config/package-lists
mkdir -p ./config/package-lists

echo "${GREEN}Ajout des package-lists ${WHITE}"
link ../../package-lists/legralNet-minimal.list.chroot		./config/package-lists/legralNet-minimal.list.chroot
link ../../package-lists/firmware.list.chroot			./config/package-lists/firmware.list.chroot
link ../../package-lists/legralNet-adminDisque.list.chroot	./config/package-lists/legralNet-adminDisque.list.chroot
link ../../package-lists/legralNet-recover.list.chroot		./config/package-lists/legralNet-recover.list.chroot
link ../../package-lists/legralNet-monitoring.list.chroot	./config/package-lists/legralNet-monitoring.list.chroot

link ../../package-lists/legralNet-nas.list.chroot		./config/package-lists/legralNet-nas.list.chroot




#cp -fl ../../package-lists/* ./config/package-lists/

echo "${GREEN}lancement de lb config${WHITE}"

echo "${CYAN}configuration des mirroirs${WHITE}"
lb config noauto \
	--cache true	--cache-indices false	--cache-packages true \
	\
 "${@}"

#	--mirror-bootstrap		http://127.0.0.1:7980/install/linux/mirroirs/sites/mirror/ftp.fr.debian.org/debian/
#	--mirror-binary			http://127.0.0.1:7980/install/linux/mirroirs/sites/mirror/ftp.fr.debian.org/debian/
#	--mirror-chroot			http://127.0.0.1:7980/install/linux/mirroirs/sites/mirror/ftp.fr.debian.org/debian/
#	"${@}"

#lb config noauto \
#	--cache true	--cache-indices false	--cache-packages true \
#	--parent-mirror-bootstrap	http://ftp.fr.debian.org/debian/ \
#	--parent-mirror-binary 		http://ftp.fr.debian.org/debian/ \
#	--parent-mirror-chroot		http://ftp.fr.debian.org/debian/ \
#	\
#	--mirror-bootstrap			http://ftp.fr.debian.org/debian/ \
#	--mirror-binary			http://ftp.fr.debian.org/debian/ \
#	--mirror-chroot			http://ftp.fr.debian.org/debian/ \
#	\
#	--parent-mirror-chroot-updates	http://ftp.fr.debian.org/debian/ \
#	--parent-mirror-chroot-backports	http://ftp.fr.debian.org/debian/ \
#	\
#	--parent-mirror-binary-security	http://ftp.fr.debian.org/debian/ \
#	--parent-mirror-binary-updates	http://ftp.fr.debian.org/debian/ \
#	--parent-mirror-binary-backports	http://ftp.fr.debian.org/debian/ \
#	"${@}"
#	--mirror-debian-installer	http://ftp.fr.debian.org/debian/ \

echo "${CYAN}configuration generale${WHITE}"
lb config noauto \
	--system live \
	--mode debian \
	-a i386 \
	--archive-areas "main contrib non-free" \
	-d squeeze \
	--binary-images  "usb-hdd" \
	--binary-filesystem "fat32" \
	--build-with-chroot true \
	--bootloader syslinux \
	--debian-installer false \
\
	--apt-recommends true \
\
	--bootappend-live "boot=live config noprompt persistence locales=fr_FR.UTF-8 keyboard-layouts=fr " \
\
	--tasks "task-french"    \
	--memtest none \
	--win32-loader false \
\
	"${@}"
#	-a i386 \
#	--tasks "ssh-server task-lxde-desktop task-french"    \
#	--archive-areas "main contrib non-free" \

########  contenu personalise du live:fin ########

