#!/bin/sh

# son rep de travail est le rep du script ./build/$PROJETNOM/ ?
echo "repertoire courant: $PWD" 

echo "${GREEN}Ajout des package-lists ${WHITE}"

packageLink firmware

packageLink legralNet-minimal;

echo "${BOLD} adminSys ${NOBOLD}"
#packageLink legralNet-adminDisque
#packageLink legralNet-monitoring
#cp -RHlnfv ../../includes.chroot/monitoring/* ./config/includes.chroot/
#packageLink legralNet-compilation


echo "${BOLD} reseau/www ${NOBOLD}"
#packageLink legralNet-nas
#packageLink legralNet-legralNet
#packageLink legralNet-lighttpd
#packageLink legralNet-piwigo
#packageLink legralNet-legralVpn


echo "${BOLD} Interface graphique ${NOBOLD}"
packageLink legralNet-X11
packageLink legralNet-openbox
#packageLink legralNet-lxde


echo "${BOLD} bureautique ${NOBOLD}"
#packageLink legralNet-bureautique
#packageLink legralNet-videos
#packageLink legralNet-videosMontages
#packageLink legralNet-blender
#packageLink legralNet-vm
#packageLink legralNet-wine

echo "${BOLD} Montage Videos ${NOBOLD}"
packageLink legralNet-videos
packageLink legralNet-videosMontages

echo "${BOLD} Jeux ${NOBOLD}"
#packageLink legralNet-jeux
#packageLink legralNet-jeux-enfant
#packageLink legralNet-jeux-simulation


echo "${BOLD} Divers ${NOBOLD}"
#packageLink legralNet-gnomeUtility
#packageLink pascal


echo "${GREEN}lancement de lb config${WHITE}"
. ../../config/lb-hdd-usb.sh
#link ../../package-lists/legralNet-videos.list.chroot		./config/package-lists/legralNet-videos.list.chroot
#link ../../package-lists/legralNet-videosMontages.list.chroot		./config/package-lists/legralNet-videosMontages.list.chroot
