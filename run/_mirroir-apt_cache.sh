#!/bin/sh
lb config noauto --cache false --cache-indices false   --cache-packages false

MIRROIR="http://127.0.0.1:3142/ftp.fr.debian.org/debian/";

lb config noauto --parent-mirror-bootstrap $MIRROIR \
                 --parent-mirror-binary    $MIRROIR \
                 --parent-mirror-chroot    $MIRROIR \
                 --mirror-bootstrap        $MIRROIR \
                 --mirror-binary           $MIRROIR \

lb config noauto --mirror-debian-installer $MIRROIR 
lb config noauto --mirror-chroot           $MIRROIR
