#!/bin/sh



###################
### PREPARATION ###
###################


### general ###

echo "${CYAN}configuration generale${WHITE}"
lb config noauto --system live --mode debian $VERBOSE 

## format du support ##
#echo "${CYAN}format du support:iso${WHITE}"

# appeller cette fonction uniquement pour i386
echo "${CYAN}Architecture: $ARCHITECTURE${WHITE}"
if [ "$ARCHITECTURE" = "i386" ]
then
    lb config noauto -d $DISTRIBUTION  --architectures "$ARCHITECTURE"
fi

echo "${CYAN}flavours: $LINUX_FLAVOURS${WHITE}"
lb config noauto -d $DISTRIBUTION  --architectures "$ARCHITECTURE" --linux-flavours "$LINUX_FLAVOURS"


echo "${CYAN}Distribution${WHITE}"
lb config noauto --archive-areas "main contrib non-free"

echo "${CYAN}backports${WHITE}"
lb config noauto --backports $isBACKPORT


echo ""
echo "${CYAN}configuration des mirroirs${WHITE}"
lb config noauto --cache $isCACHE --cache-indices $isCACHE_INDICES --cache-packages $isCACHE_PACKAGES
#loadConfig _mirroir-apt_cache


echo ""
echo "${CYAN}format du support:hdd-usb${WHITE}"
#hdd-usb
 lb config noauto --binary-images  "$BINARY_IMAGES"  --binary-filesystem "$BINARY_FILESYSTEM" --bootloader $BOOTLOADER --win32-loader "$WIN32_LOADER"
#iso
#lb config noauto --binary-images  "iso" --binary-filesystem "fat32"  --bootloader syslinux --win32-loader true


echo ""
echo "${CYAN}Ajout de memtest${WHITE}"
lb config noauto --memtest $isMEMTEST


### live-config ###
echo "${CYAN}preparation de --bootappend-live${WHITE}"

lb config noauto --bootappend-live "boot=live $LIVE_BOOT_PARAM $LIVE_CONFIG_PARAM"

