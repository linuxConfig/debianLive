#!/bin/sh
scriptPath=`realpath $0`;         # chemin  absolu complet du script rep + nom
scriptRep=`dirname $scriptPath`;  # repertoire absolu du script (pas de slash de fin)
scriptFileName=${0##*/};          # nom.ext
scriptNom=${scriptFileName%.*}    # uniquement le nom
PLAYLISTS_ROOT="$scriptRep/playlists" # chemin relatif au script

VERSION="2.0.0"

#http://www.commentcamarche.net/faq/6458-la-commande-tput
#http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/x405.html

BOLD=`tput smso`
NOBOLD=`tput rmso`

BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
CYAN=`tput setaf 4`
COUL=`tput setaf 5`
BLUE=`tput setaf 6`
WHITE=`tput setaf 7`

CMD=$BLUE;
NORMAL=$WHITE;


# #################### #
# functions generiques #
# #################### #
evalCmd (){
    cmd=$1;
    echo $CMD $cmd $NORMAL;
    eval $cmd;
}


# ################### #
# fonctions du projet #
# ################### #
# charge un fichier contenu dans le repertoire ./run
# ATTENTION: son rep de travail est le rep du script ./build/$PROJETNOM/ 
. ./fonctions.sh


#clear;
echo "$GREEN################################################"
echo "config4Live version: ${VERSION}"
echo "N################################################$NORMAL"

if [ $# = 0 ];then
	echo ""
	echo "${GREEN}usage: $0 --list $NORMAL ";
	echo "${GREEN}usage: $0 --rename projet $NORMAL ";
	echo "${GREEN}usage: $0 projet(sans .sh) $NORMAL ";
        echo "Doit etre executer dans le repertoire ou se situe ce fichier"
	exit
fi


if [ "$1" = "--list" ]
then
   echo "Projets disponibles:"
   ls -a ./run
   exit
fi


if [ "$1" = "--rename" ]
then
    if [ $# -lt 2 ];then
	echo ""
	echo "${GREEN}usage: $0 --rename projet $NORMAL # nom de projet obligatoire";
	exit
    fi

    PROJETNOM=$2

    loadParam

    echo "renomme les binaires"
    renommer $2
    ls -a ./binaires
    exit
fi


PROJETNOM=$1
UTILISATEUR=$PROJETNOM
#if [ $# = 2 ];then
#	UTILISATEUR=$2
#fi
#"echo "utilisateur:"$UTILISATEUR


echo "$BOLD PROJETNOM:" $PROJETNOM $NOBOLD
LIVEPATH=`dirname $0`
cd  $LIVEPATH
echo "repertoire courant: $BLUE$PWD$WHITE" 






#le fichier de configuration existe t'il?
loadConfig


#le fichier de configuration existe t'il?
echo "$GREEN creation du repertoire du cache $WHITE"
if [ ! -d ./cache ]; then
    evalCmd " mkdir ./cache/";
fi

echo "";
echo "";

#################################################################"
echo "$BOLD $YELLOW préparation du projet $NOBOLD $WHITE"

#mkdir -p /dev/shm/liveBuild/ && BUILDLOG=/dev/shm/liveBuild/$PROJETNOM.log
BUILDLOG=/dev/null #peut etre surcharger par ./run/$PROJETNOM.sh
BUILDLOG=./build.log

echo 
PROJETBUILDPATH="$PWD/build/${PROJETNOM}/"
echo "PROJETBUILDPATH:" $BLUE$PROJETBUILDPATH$WHITE
echo "configuration man lb_build"
echo "configuration http://legral.fr/legralNet/legralNet-0.2/?page=tutoriels&tutoriels=debianLive=accueil"
echo "connaitre les task: apt-cache search --names-only ^task-"
echo 

# - Nettoyage - #
echo "$GREEN Suppression du repertoire build du projet: $PROJETNOM  $WHITE"
evalCmd " rm -R $PROJETBUILDPATH";

echo "$GREEN creation et entrer dans le repertoire de travail $BLUE$PROJETBUILDPATH $WHITE"
evalCmd " mkdir -p $PROJETBUILDPATH";
evalCmd " cd $PROJETBUILDPATH";

echo "repertoire courant: $PWD" #./build/$PROJETNOM/

echo "$GREEN création des repertoires internes$WHITE"
evalCmd " lb config";
echo "";

echo "$GREEN création du lien vers le cache commmun $WHITE"
evalCmd " ln -s ../../cache";

echo "$GREEN changement des droits $WHITE"
evalCmd " chmod -R 777 ./";
echo "";
echo "";

#################################################################
echo "$BOLD$YELLOW ------- lancement du script de configuration ----- $NOBOLD$WHITE"
echo 'Chargement des parametres:'
evalCmd ". ../../run/$PROJETNOM-param.sh"
evalCmd " . ../../prepare-pre.sh";
evalCmd " . ../../run/$PROJETNOM.sh";
evalCmd " . ../../prepare-post.sh";
echo "";
echo "";

#################################################################
echo "$BOLD$YELLOW ------- creation du script de construction ----- $NOBOLD$WHITE"


SCRIPTBUILDPATH="${PROJETBUILDPATH}${PROJETNOM}-build.sh"
#echo "cd $PWD && time lb build noauto 2>&1 | tee $BUILDLOG"
echo "#!/bin/sh" > $SCRIPTBUILDPATH
echo "BOLD=`tput smso`">> $SCRIPTBUILDPATH
echo "NOBOLD=`tput rmso`">> $SCRIPTBUILDPATH
echo "BLACK=`tput setaf 0`">> $SCRIPTBUILDPATH
echo "RED=`tput setaf 1`">> $SCRIPTBUILDPATH
echo "GREEN=`tput setaf 2`">> $SCRIPTBUILDPATH
echo "YELLOW=`tput setaf 3`">> $SCRIPTBUILDPATH
echo "CYAN=`tput setaf 4`">> $SCRIPTBUILDPATH
echo "COUL=`tput setaf 5`">> $SCRIPTBUILDPATH
echo "BLUE=`tput setaf 6`">> $SCRIPTBUILDPATH
echo "WHITE=`tput setaf 7`">> $SCRIPTBUILDPATH

echo "couleurNORMAL=$WHITE;couleurINFO=$BLUE;couleurCOM=$YELLOW;couleurWARN=$RED;couleurTITRE1=$GREEN;couleurTITRE2=$MAGENTA" >> $SCRIPTBUILDPATH
echo "export BOLD;export NOBOLD;export BLACK;export RED;export GREEN;export YELLOW;export CYAN;export MAGENTA;export BLUE;export WHITE;" >> $SCRIPTBUILDPATH
echo "export couleurNORMAL;export couleurINFO;export couleurCOM;export couleurWARN;export couleurTITRE1;export couleurTITRE2;" >> $SCRIPTBUILDPATH

echo "#generer automatiquement  a chaque configuration" >>$SCRIPTBUILDPATH
echo "echo " >> $SCRIPTBUILDPATH
#echo "echo '========================================'" >> $SCRIPTBUILDPATH
#cho 'ls -l ./' >> $SCRIPTBUILDPATH
#echo 'echo "$GREEN aller dans le repertoire de travail: `dirname $0` $WHITE"' >> $SCRIPTBUILDPATH
echo 'cd `dirname $0`' >> $SCRIPTBUILDPATH
echo 'echo "repertoire courant: $GREEN $PWD $WHITE"' >> $SCRIPTBUILDPATH

echo 'evalCmd (){cmd=$1;echo $CMD $cmd $NORMAL;eval $cmd;}' >> $SCRIPTBUILDPATH

echo 'echo "$YELLOW debut de la construction du live $WHITE"' >> $SCRIPTBUILDPATH
echo "lb build noauto 2>&1  | tee $BUILDLOG" >> $SCRIPTBUILDPATH

echo 'echo ""' >> $SCRIPTBUILDPATH
echo 'echo "$YELLOW FIN de la construction du live $WHITE"' >> $SCRIPTBUILDPATH

echo 'echo ""' >> $SCRIPTBUILDPATH
echo 'echo "$YELLOW Changement des droits du repertoire build $WHITE"' >> $SCRIPTBUILDPATH
echo 'chmod -R 777 ./'  >> $SCRIPTBUILDPATH

echo 'echo "$YELLOW listing du repertoire du build $WHITE"' >> $SCRIPTBUILDPATH
echo 'ls -la ./'  >> $SCRIPTBUILDPATH

echo 'echo ""' >> $SCRIPTBUILDPATH
echo "echo \"$YELLOW Affichage de $BUILDLOG $WHITE\"" >> $SCRIPTBUILDPATH
echo "less $BUILDLOG"  >> $SCRIPTBUILDPATH


#echo ''  >> $SCRIPTBUILDPATH

#echo 'echo "$BOLD$YELLOW #########################################"'  >> $SCRIPTBUILDPATH
echo 'echo "$NOBOLD$WHITE"'  >> $SCRIPTBUILDPATH

#-------------------------------------------------
evalCmd "chmod +x $SCRIPTBUILDPATH";

eval ''
echo "$YELLOW pour lancer la construction (en root):"
echo "$BLUE $SCRIPTBUILDPATH$WHITE"
echo ''

exit 0

