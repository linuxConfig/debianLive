#!/bin/sh

# ################### #
# fonctions du projet #
# ################### #

loadConfig (){
    #evalCmd ". ../../run/$1.sh";
    #le fichier de configuration existe t'il?
    echo "$GREEN Tester la presence de ./run/$PROJETNOM.sh et le rendre executable $WHITE"
    if [ -f ./run/$PROJETNOM.sh ]; then
        echo "le fichier de configuration existe"
        evalCmd " chmod +x ./run/$PROJETNOM.sh";
        return 1
    else
        echo "$RED le fichier de configuration ./run/$PROJETNOM.sh n'existe pas!$WHITE"
        exit
    fi
}

loadParam (){
    #evalCmd ". ../../run/$1.sh";
    #le fichier de parametre du projet existe t'il?
    echo "$GREEN Tester la presence de ./run/${PROJETNOM}-param.sh et le rendre executable $WHITE"
    if [ -f ./run/${PROJETNOM}-param.sh ]; then
        echo "le fichier de configuration existe"
        evalCmd " chmod +x ./run/${PROJETNOM}-param.sh";
        . ./run/${PROJETNOM}-param.sh
        return 1
    else
        echo "$RED le fichier de configuration ./run/${PROJETNOM}-param.sh n'existe pas!$WHITE"
        exit
    fi
}


#renomme les fichiers binaire du projet
#$1 nom du projet # ne pas utiliser PROJETNOM car non defini lors de l'appel
renommer(){
    PROJET=$1

    evalCmd "mkdir -p ./binaires/$PROJET/"

    f="./build/$PROJET/binary/live/vmlinuz"
    if [ -f "$f" ]
    then
        evalCmd "mv $f ./binaires/$PROJET/$VMLINUZ_NAME"
    fi

    f="./build/$PROJET/binary/live/initrd.img"
    if [ -f "$f" ]
    then
        evalCmd "mv $f ./binaires/$PROJET/$INITRD_NAME"
    fi
    # deplacement des autres s'il y ena
    evalCmd "mv ./build/$PROJET/binary/live/initrd.img-* ./binaires/$PROJET/"

    f="./build/$PROJET/binary/live/filesystem.squashfs"
    if [ -f "$f" ]
    then
        evalCmd "mv $f ./binaires/$PROJET/$SQUASHFS_NAME"
    fi
    # deplacement des autres s'il y ena
    evalCmd "mv ./build/$PROJET/binary/live/vmlinuz-* ./binaires/$PROJET/"

    evalCmd "ls -l ./binaires/$PROJET"

    # deplacement du log
    evalCmd "mv $BUILDLOG ./binaires/$PROJET"

}

# copie 1 fichier 
add_template_lib(){ # 'public'|'private' libNom structure fileNom

    # verfier nb param >= 3

    # psp #
    confidentialite=$1
    libNom=$2
    structure=$3
    fileNom=$4
    
    #                              public          legralNet   hooks    1002-legralNet.hook.chroot
    filePath="../../templates/$confidentialite/lib/$libNom/$structure/$fileNom"
   if [ -f "$filePath" ]
   then
       evalCmd "cp $filePath ./config/$structure/$fileNom; #add_template_lib";
   else
       echo "${GREEN} $1 $RED $filePath inexistant$WHITE"
   fi
}


# copie le repertoire de l'utilisateur
add_template_theme(){ # 'public'|'private' theme

    # verfier nb param >= 2

    # psp #
    confidentialite=$1
    theme=$2
    
    #                              public    /      / legralNet-base   
    dirPath="../../templates/$confidentialite/themes/$theme"

    if [ -d "$dirPath" ]
    then
        evalCmd "cp  -RHlnf $dirPath/* ./config/;#add_template_theme";
    else
        echo "${GREEN} $1 $RED $dirPath inexistant$WHITE"
    fi
}

	     
# copie le repertoire de l'utilisateur
add_template_home_utilisateurs() # 'public'|'private' userName
{
    # verfier nb param >= 2

    # psp #
    confidentialite=$1
    userName=$2
    
    #                              public    /                 /   user   
    dirPath="../../templates/$confidentialite/home-utilisateurs/$userName"

    if [ -d "$dirPath" ]
    then
        evalCmd "cp  -RHlnf $dirPath/* ./config/;#add_template_home_utilisateurs";
    else
        echo "${GREEN} $1 $RED $dirPath inexistant$WHITE"
    fi
}


# copie le repertoire d'un add-on de l'utilisateur
add_template_home_lib() # 'public'|'private' userName
{
    # verfier nb param >= 3

    # psp #
    confidentialite=$1
    userName=$2
    libNom=$3

    #                              public    /        /   user   / user-autologin-tty1/
    dirPath="../../templates/$confidentialite/home-lib/$userName/$libNom/"

    if [ -d "$dirPath" ]
    then
        evalCmd "cp  -RHlnf $dirPath/* ./config/; #add_template_home_lib";
    else
        echo "${GREEN} $1 $RED $dirPath inexistant$WHITE"
    fi
}



